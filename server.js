const { DateTimeFormatter } = require("@js-joda/core");
const { LocalDate } = require("@js-joda/core");
const Progress = require("progress");
const axios = require("axios");
const blessed = require("blessed");
const chalk = require("chalk");
const columnify = require("columnify");
const fs = require("fs");
const htmlToText = require("html-to-text");
const inquirer = require("inquirer");
const logSymbols = require("log-symbols");
const ora = require("ora");
const readline = require("readline");
const treeify = require("treeify");

const builds = [];
const buildsProgress = new Progress(
  `${logSymbols.info} ${chalk.magenta(
    "Searching builds..."
  )} :bar :current/:total`,
  { total: 1, width: process.stdout.columns }
);
const games = [];
const gamesProgress = new Progress(
  `${logSymbols.info} ${chalk.magenta(
    "Searching games..."
  )} :bar :current/:total`,
  { total: 1, width: process.stdout.columns }
);
const ORA = ora();

function goodOldGamesChangelog(game) {
  const screen = blessed.screen({ smartCSR: true });
  const text = blessed.box({
    alwaysScroll: true,
    content: htmlToText.fromString(game.changelog, {
      wordwrap: screen.width - 1
    }),
    height: "100%-2",
    keys: true,
    scrollable: true,
    scrollbar: { style: { bg: "white" } },
    width: "100%"
  });
  text.key("escape", () => {
    screen.destroy();
    process.exit();
  });
  screen.append(text);
  screen.append(
    blessed.text({ bottom: "0", content: "Press ESC to exit.", width: "100%" })
  );
  screen.render();
}

function goodOldGamesCli() {
  inquirer
    .prompt([
      {
        choices: ["Search", "Update", "Exit"],
        message: "Action:",
        name: "action",
        type: "list"
      },
      {
        message: "Search:",
        name: "search",
        when: (answers) => answers.action === "Search"
      }
    ])
    .then((answers) => {
      console.log();
      builds.length = 0;
      games.length = 0;
      switch (answers.action) {
        case "Search":
          gamesProgress.complete = true;
          gamesProgress.curr = 0;
          gamesProgress.total = 1;
          gamesProgress.render();
          goodOldGamesSearch(answers.search);
          break;
        case "Update":
          buildsProgress.complete = true;
          buildsProgress.curr = 0;
          buildsProgress.total = 1;
          buildsProgress.render();
          goodOldGamesUpdate();
          break;
        default:
          process.exit();
      }
    });
}

function goodOldGamesError(error) {
  ORA.fail(`${chalk.bold.red(error)}\n`);
  goodOldGamesCli();
}

function goodOldGamesGame(id) {
  ORA.start(chalk.magenta("Getting information..."));
  axios
    .get(
      `https://api.gog.com/products/${id}?expand=changelog,description,downloads,expanded_dlcs`
    )
    .then((response) => {
      ORA.succeed(`${ORA.text}\n`);
      goodOldGamesMenu(response.data);
    })
    .catch(goodOldGamesError);
}

function goodOldGamesList() {
  console.log();
  inquirer
    .prompt([
      {
        choices: games
          .map((currentValue) => ({
            name: currentValue.title,
            value: currentValue.id
          }))
          .sort((a, b) => a.name.localeCompare(b.name))
          .concat([new inquirer.Separator(), "Back", new inquirer.Separator()]),
        message: "Game:",
        name: "game",
        type: "list"
      }
    ])
    .then((answers) => {
      console.log();
      switch (answers.game) {
        case "Back":
          goodOldGamesCli();
          break;
        default:
          goodOldGamesGame(answers.game);
      }
    });
}

function goodOldGamesMenu(game) {
  console.log(
    "%s  \n%s\n",
    chalk.bold(game.title),
    htmlToText.fromString(game.description.lead, {
      wordwrap: process.stdout.columns
    })
  );
  inquirer
    .prompt([
      {
        choices: ["Show Changelog", "Show Files Tree", "Back"],
        message: "Action:",
        name: "action",
        type: "list"
      }
    ])
    .then((answers) => {
      console.log();
      switch (answers.action) {
        case "Show Changelog":
          goodOldGamesChangelog(game);
          break;
        case "Show Files Tree":
          goodOldGamesTree(game);
          break;
        default:
          goodOldGamesCli();
      }
    });
}

function goodOldGamesOutdated() {
  console.log(
    "\n  %s\n%s\n",
    chalk.bold("Outdated games:"),
    columnify(
      games
        .filter((game) => {
          const build = builds.find((build) => build.product_id === game.id);
          return (
            LocalDate.parse(
              build.date_published,
              DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssX")
            ).isAfter(LocalDate.parse(game.date_published)) ||
            (build.version_name || "N/A") !== game.version_name
          );
        })
        .map((game) => {
          const build = builds.find((build) => build.product_id === game.id);
          return [
            null,
            null,
            game.title,
            ":",
            chalk.red(game.version_name),
            chalk.red(`(${game.date_published})`),
            "->",
            chalk.green(build.version_name || "N/A"),
            chalk.green(
              `(${LocalDate.parse(
                build.date_published,
                DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssX")
              )})`
            )
          ];
        }),
      { showHeaders: false }
    )
  );
  goodOldGamesCli();
}

function goodOldGamesSearch(search, page) {
  axios
    .get(
      `https://www.gog.com/games/ajax/filtered?hide=dlc&mediaType=game&page=${
        page || 1
      }&search=${search}&sort=title`
    )
    .then((response) => {
      response.data.products.forEach((product) => games.push(product));
      if (gamesProgress.complete) {
        gamesProgress.callback = goodOldGamesList;
        gamesProgress.complete = false;
        gamesProgress.total = response.data.totalPages;
        for (let page = 2; page <= gamesProgress.total; page++) {
          goodOldGamesSearch(search, page);
        }
      }

      gamesProgress.tick();
    })
    .catch(goodOldGamesError);
}

function goodOldGamesTree(game) {
  ORA.warn(chalk.yellow("Not Yet Implemented"));
  const installers = game.downloads.installers.filter(
    (installer) => installer.language === "en" && installer.os === "windows"
  );
  const dlcsIntallers = game.expanded_dlcs
    .map((expandedDlc) => expandedDlc.downloads)
    .flatMap((download) => download.installers)
    .filter(
      (installer) => installer.language === "en" && installer.os === "windows"
    );
  console.log(
    treeify.asTree(
      {
        total: installers
          .concat(dlcsIntallers)
          .flatMap((installer) => installer.files).length,
        game: installers.reduce((o, installer) => {
          o[installer.name] = installer.files.length;
          return o;
        }, {}),
        dlcs: dlcsIntallers.reduce((o, installer) => {
          o[installer.name] = installer.files.length;
          return o;
        }, {})
      },
      true
    )
  );
  goodOldGamesMenu(game);
}

function goodOldGamesUpdate() {
  readline
    .createInterface({ input: fs.createReadStream("goggames.txt") })
    .on("close", () => {
      buildsProgress.callback = goodOldGamesOutdated;
      buildsProgress.complete = false;
      buildsProgress.total = games.length;
      games.forEach((game) =>
        axios
          .get(
            `https://content-system.gog.com/products/${game.id}/os/windows/builds?generation=2`
          )
          .then((response) => {
            builds.push(
              response.data.items.reduce((item1, item2) => {
                const datePublished1 = LocalDate.parse(
                  item1.date_published,
                  DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssX")
                );
                const datePublished2 = LocalDate.parse(
                  item2.date_published,
                  DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssX")
                );
                return datePublished1.isAfter(datePublished2) ||
                  (datePublished1.equals(datePublished2) &&
                    item1.version_name > item2.version_name)
                  ? item1
                  : item2;
              })
            );
            buildsProgress.tick();
          })
          .catch(goodOldGamesError)
      );
    })
    .on("line", (input) => {
      const game =
        /^(?<title>[\S ]+)\t+\|\t+(?<slug>\S+)\t+\|\t+(?<version_name>[\S ]+)\t+\|\t+(?<date_published>\d{4}-\d{2}-\d{2})\t+\|\t+(?<id>\d+)\t+\|\t+(.+)/.exec(
          input
        );
      if (game) {
        games.push(game.groups);
      } else {
        ORA.warn(chalk.yellow(input));
      }
    });
}

goodOldGamesCli();
